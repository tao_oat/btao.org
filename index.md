---
title: /home
layout: home
permalink: /
---

Hey, I'm Tao (he/him). I'm into security, philosophy, and using technology
for social good, and I'm currently a software engineer at [Intruder](https://www.intruder.io/).

Find me on [GitLab](https://gitlab.com/tao_oat/), or subscribe to my [RSS feed](/feed.xml).

