---
title: eusay
subtitle: An online democracy platform for student unions.
code: https://github.com/HughMcGrade/eusay
date: 2014-11-01
importance: 1
---

An online voting and discussion platform for student unions. This project won a local hackathon, was selected for the Jisc Summer of Student Innovation competition where it won funding, and was later bought by my student union and deployed for all 30,000 students.
