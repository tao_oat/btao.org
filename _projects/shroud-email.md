---
title: Shroud.email
subtitle: Email privacy service
image: /static/img/shroud.png
wide: true
link: https://shroud.email/
code: https://gitlab.com/shroud/shroud.email
date: 2021-12-05
importance: 6
---
Your email address is your *de facto* ID on the web. Shroud.email helps protect it.

By creating aliases that forward to your real email address, spammers and marketers aren't able to
link your behaviour on one site to another. In addition to hiding your real email address, these aliases
also remove creepy trackers.
