---
title: fediverse.space
subtitle: A tool to visualize decentralized social networks.
image: /static/img/fediverse-space-header.png
wide: true
link: https://fediverse.space/
code: https://gitlab.com/tao_oat/fediverse.space
date: 2018-09-05
importance: 4
---

The “fediverse” is a term used to describe the set of social media servers implementing the decentralized ActivityPub protocol. There are many implementations, the most famous being Mastodon, and these can all communicate with each other. Together, these servers are creating a libre, decentralized, and more democratic alternative to social networks like Twitter.

fediverse.space is a tool to visualize communities in the fediverse. All known servers are presented as nodes on a graph, the edges between them being weighted by the amount of interaction between users on these servers. Clusters quickly emerge – for instance, servers with the same primary language will be close to each other as, for example, French speakers tend to interact with other French speakers. There are more subtle groupings, too: topics of discussion, types of users (serious vs. ironic), and political leanings all play a role.

While interesting in its own right, such a graph or “map” of the fediverse can also be used to combat online abuse. If you encounter a server that, for instance, harbors users espousing hate speech without being moderated, a tool of this sort can reveal the servers it is closest to, which are more likely to have similarly laissez-faire moderation policies.
