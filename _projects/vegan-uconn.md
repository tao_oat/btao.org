---
title: Vegan UConn
subtitle: An easy way to find vegan options at the dining halls and cafés at the University of Connecticut.
code: https://gitlab.com/tao_oat/vegan-uconn
link: https://veganuconn.com/
date: 2015-12-29
importance: 2
---

I’m an advocate of animal rights and believe that veganism is the best way to put this belief into action. My preferred form of advocacy is to make veganism as accessible as possible.

The dining halls at the University of Connecticut have excellent vegan options, but each individual canteen may not have something every day. The online menus are clunky, requiring many clicks to see what’s available across campus. Hearing my vegan friends complain about this, I built a webapp presenting these options in an interface that prioritizes user experience. It’s quick to see what’s available, and where, for your next meal.

Several years later, this webapp still has regular users (what I believe to be the majority of vegans at the university) and is seeing slow but steady growth.
