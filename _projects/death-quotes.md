---
title: Death Quotes
subtitle: An Amazon Echo skill that gives you a quote about death.
code: https://gitlab.com/tao_oat/alexa-death-quotes
date: 2017-07-11
importance: 3
---

Death Quotes is an app for the Amazon Echo. Upon receiving a request like “Alexa, ask Death Quotes to tell me about death,” Alexa will read a random quote on the subject of our mortality.

Partially inspired by Ernst Becker’s book <i>The Denial of Death</i>, this project aims to revive the Renaissance mantra of <i>memento mori</i>, but adjusted to the contemporary technology consumer.
